package ru.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.IRepository;
import ru.volkova.tm.api.IService;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.entity.AbstractEntity;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;

import java.util.List;

import java.util.Optional;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
