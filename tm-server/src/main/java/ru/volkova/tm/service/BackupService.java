package ru.volkova.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.service.IBackupService;
import ru.volkova.tm.api.service.ServiceLocator;
import ru.volkova.tm.dto.Domain;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class BackupService implements IBackupService {

    @NotNull
    protected static final String FILE_BACKUP = "./backup.xml";

    @Nullable
    protected ServiceLocator serviceLocator;

    public BackupService(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @SneakyThrows
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        if (serviceLocator == null) throw new ObjectNotFoundException();
        final String userId = serviceLocator.getAuthService().getUserId();
        domain.setProjects(serviceLocator.getProjectService().findAll(userId));
        domain.setTasks(serviceLocator.getTaskService().findAll(userId));
        domain.setUsers(serviceLocator.getAdminUserService().findAll());
        return domain;
    }

    @SneakyThrows
    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        if (serviceLocator == null) throw new ObjectNotFoundException();
        final String userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getProjectService().clear(userId);
        serviceLocator.getProjectService().addAll(domain.getProjects());
        serviceLocator.getTaskService().clear(userId);
        serviceLocator.getTaskService().addAll(domain.getTasks());
        serviceLocator.getAdminUserService().clear();
        serviceLocator.getAdminUserService().addAll(domain.getUsers());
    }

    @Override
    @SneakyThrows
    public void load() {
        @NotNull final File file = new File(FILE_BACKUP);
        if (!file.exists()) return;
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(FILE_BACKUP)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void save() {
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_BACKUP);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }


}
