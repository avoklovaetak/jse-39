package ru.volkova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private final Properties properties = new Properties();

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password_secret";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password_iteration";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "version";

    @NotNull
    private static final String APPLICATION_VERSION_DEFAULT = "";

    @NotNull
    private static final String APPLICATION_DEVELOPER_NAME_KEY = "application_developer";

    @NotNull
    private static final String APPLICATION_DEVELOPER_NAME_DEFAULT = "";

    @NotNull
    private static final String APPLICATION_DEVELOPER_EMAIL_KEY = "application_email";

    @NotNull
    private static final String APPLICATION_DEVELOPER_EMAIL_DEFAULT = "";

    @NotNull
    private static final String SERVER_HOST_KEY = "server_host";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "";

    @NotNull
    private static final String SERVER_PORT_KEY = "server_port";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "";

    @NotNull
    public static final String JDBC_DRIVER = "jdbc.driver";

    @NotNull
    public static final String JDBC_DRIVER_DEFAULT = "com.mysql.jdbc.Driver";

    @NotNull
    public static final String JDBC_USER = "jdbc.user";

    @NotNull
    public static final String JDBC_USER_DEFAULT = "root";

    @NotNull
    public static final String JDBC_PASSWORD = "jdbc.password";

    @NotNull
    public static final String JDBC_PASSWORD_DEFAULT = "1905";

    @NotNull
    private static final String JDBC_URL = "jdbc.url";

    @NotNull
    private static final String JDBC_URL_DEFAULT = "jdbc:mysql://localhost:3306/task-manager";

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        if (System.getProperties().containsKey(PASSWORD_SECRET_KEY)) return System.getProperty(PASSWORD_SECRET_KEY);
        if (System.getenv().containsKey(PASSWORD_SECRET_KEY)) return System.getenv(PASSWORD_SECRET_KEY);
        return properties.getProperty(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        if (System.getProperties().containsKey(PASSWORD_ITERATION_KEY)){
            final String value = System.getProperty(PASSWORD_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        if (System.getenv().containsKey(PASSWORD_ITERATION_KEY)){
            final String value = System.getenv(PASSWORD_ITERATION_KEY);
            return Integer.parseInt(value);
        }
        final String value = properties.getProperty(PASSWORD_ITERATION_KEY,PASSWORD_ITERATION_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        if (System.getProperties().containsKey(APPLICATION_VERSION_KEY))
            return System.getProperty(APPLICATION_VERSION_KEY);
        if (System.getenv().containsKey(APPLICATION_VERSION_KEY))
            return System.getenv(APPLICATION_VERSION_KEY);
        return properties.getProperty(APPLICATION_VERSION_KEY, APPLICATION_VERSION_DEFAULT);
    }

    @NotNull
    @Override
    public String getDeveloperName() {
        if (System.getProperties().containsKey(APPLICATION_DEVELOPER_NAME_KEY))
            return System.getProperty(APPLICATION_DEVELOPER_NAME_KEY);
        if (System.getenv().containsKey(APPLICATION_DEVELOPER_NAME_KEY))
            return System.getenv(APPLICATION_DEVELOPER_NAME_KEY);
        return properties.getProperty(APPLICATION_DEVELOPER_NAME_KEY, APPLICATION_DEVELOPER_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getDeveloperEmail() {
        if (System.getProperties().containsKey(APPLICATION_DEVELOPER_EMAIL_KEY))
            return System.getProperty(APPLICATION_DEVELOPER_EMAIL_KEY);
        if (System.getenv().containsKey(APPLICATION_DEVELOPER_EMAIL_KEY))
            return System.getenv(APPLICATION_DEVELOPER_EMAIL_KEY);
        return properties.getProperty(APPLICATION_DEVELOPER_EMAIL_KEY, APPLICATION_DEVELOPER_EMAIL_DEFAULT);
    }

    @NotNull
    @Override
    public String getServerHost() {
        if (System.getProperties().containsKey(SERVER_HOST_KEY))
            return System.getProperty(SERVER_HOST_KEY);
        if (System.getenv().containsKey(SERVER_HOST_KEY))
            return System.getenv(SERVER_HOST_KEY);
        return properties.getProperty(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public String getServerPort() {
        if (System.getProperties().containsKey(SERVER_PORT_KEY))
            return System.getProperty(SERVER_PORT_KEY);
        if (System.getenv().containsKey(SERVER_PORT_KEY))
            return System.getenv(SERVER_PORT_KEY);
        return properties.getProperty(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

    public static void main(String[] args) {
        final PropertyService propertyService = new PropertyService();
        System.out.println(propertyService.getPasswordSecret());
    }

    @NotNull
    @Override
    public String getJdbcUrl() {
        return properties.getProperty(JDBC_URL, JDBC_URL_DEFAULT);
    }

    @NotNull
    @Override
    public String getJdbcDriver() {
        return properties.getProperty(JDBC_DRIVER, JDBC_DRIVER_DEFAULT);
    }

    @NotNull
    @Override
    public String getJdbcUser() {
        return properties.getProperty(JDBC_USER, JDBC_USER_DEFAULT);
    }

    @NotNull
    @Override
    public String getJdbcPassword() {
        return properties.getProperty(JDBC_PASSWORD, JDBC_PASSWORD_DEFAULT);
    }

}
