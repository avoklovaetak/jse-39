package ru.volkova.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.other.ISaltSetting;

public interface IPropertyService extends ISaltSetting {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getDeveloperName();

    @NotNull
    String getDeveloperEmail();

    @NotNull
    String getServerHost();

    @NotNull
    String getServerPort();

    @NotNull
    String getJdbcUrl();

    @NotNull
    String getJdbcDriver();

    @NotNull
    String getJdbcUser();

    @NotNull
    String getJdbcPassword();


}
