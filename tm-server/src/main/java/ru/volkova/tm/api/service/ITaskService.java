package ru.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.IService;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.enumerated.Status;

import java.util.List;

public interface ITaskService extends IService<Task> {

    Task insert(@NotNull Task task);

    void add(
            @NotNull String userId,
            @Nullable String name,
            @Nullable String description
    );

    void addAll(@Nullable List<Task> entities);

    void clear(@NotNull String userId);

    List<Task> findAll(@NotNull String userId);

    @Nullable
    Task findById(@NotNull final String userId,@NotNull final String id);

    @Nullable
    Task findOneByIndex(@NotNull String userId,@NotNull Integer index);

    @Nullable
    Task findOneByName(@NotNull String userId,@NotNull String name);

    @Nullable
    Task changeOneStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @Nullable final Status status
    );

    @Nullable
    Task changeOneStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @Nullable final Status status
    );

    void removeById(@NotNull String userId,@NotNull String id);

    void removeOneByName(@NotNull String userId,@Nullable String name);

    @Nullable
    Task updateOneById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    );

}
