package ru.volkova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.IRepository;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.enumerated.Status;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @Insert("INSERT INTO 'task' ('id','name','description,'" +
            "'project_id','date_start','date_end','created','status','user_id'" +
            "VALUES (#{id},#{name},#{description},#{projectId}," +
            "#{dateStart},#{dateFinish},#{created},#{status},#{userId})")
    @Nullable
    Task insert(@Nullable final Task task);

    @Delete("DELETE FROM 'task'")
    void clear(@NotNull String userId);

    @Select("SELECT * FROM 'task' WHERE user_id = #{userId}")
    @Result(column = "id", property = "id")
    @Result(column = "name", property = "name")
    @Result(column = "description", property = "description")
    @Result(column = "project_id", property = "projectId")
    @Result(column = "date_start", property = "dateStart")
    @Result(column = "date_end", property = "dateFinish")
    @Result(column = "created", property = "created")
    @Result(column = "status", property = "status")
    @Result(column = "user_id", property = "userId")
    @NotNull
    List<Task> findAll(@NotNull String userId);

    @Nullable
    Task add(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    );

    @Update("UPDATE TABLE 'task' SET project_id = #{projectId} WHERE id = #{taskId} AND user_id = #{userId}")
    void bindTaskByProjectId(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String taskId
    );

    @Select("SELECT * FROM 'task' WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Result(column = "id", property = "id")
    @Result(column = "name", property = "name")
    @Result(column = "description", property = "description")
    @Result(column = "project_id", property = "projectId")
    @Result(column = "date_start", property = "dateStart")
    @Result(column = "date_end", property = "dateFinish")
    @Result(column = "created", property = "created")
    @Result(column = "status", property = "status")
    @Result(column = "user_id", property = "userId")
    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId,@NotNull String projectId);

    @Delete("DELETE FROM 'task' WHERE user_id = #{userId} AND project_id = #{projectId}")
    void removeAllByProjectId(@NotNull String userId,@NotNull String projectId);

    @Update("UPDATE TABLE 'task' SET project_id = #{projectId} "+
            "WHERE id = #{taskId} AND user_id = #{userId}")
    void unbindTaskByProjectId(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String taskId
    );

    @Select("SELECT * FROM 'task' WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Result(column = "id", property = "id")
    @Result(column = "name", property = "name")
    @Result(column = "description", property = "description")
    @Result(column = "project_id", property = "projectId")
    @Result(column = "date_start", property = "dateStart")
    @Result(column = "date_end", property = "dateFinish")
    @Result(column = "created", property = "created")
    @Result(column = "status", property = "status")
    @Result(column = "user_id", property = "userId")
    @Nullable
    Task findById(@NotNull final String userId,@NotNull final String id);

    @Select("SELECT * FROM 'task' WHERE user_id = #{userId} LIMIT #{index},1")
    @Result(column = "id", property = "id")
    @Result(column = "name", property = "name")
    @Result(column = "description", property = "description")
    @Result(column = "project_id", property = "projectId")
    @Result(column = "date_start", property = "dateStart")
    @Result(column = "date_end", property = "dateFinish")
    @Result(column = "created", property = "created")
    @Result(column = "status", property = "status")
    @Result(column = "user_id", property = "userId")
    @Nullable
    Task findOneByIndex(@NotNull String userId,@NotNull Integer index);

    @Select("SELECT * FROM 'task' WHERE name = #{name} AND user_id = #{userId} LIMIT 1")
    @Result(column = "id", property = "id")
    @Result(column = "name", property = "name")
    @Result(column = "description", property = "description")
    @Result(column = "project_id", property = "projectId")
    @Result(column = "date_start", property = "dateStart")
    @Result(column = "date_end", property = "dateFinish")
    @Result(column = "created", property = "created")
    @Result(column = "status", property = "status")
    @Result(column = "user_id", property = "userId")
    @Nullable
    Task findOneByName(@NotNull String userId,@NotNull String name);

    @Update("UPDATE TABLE 'task' SET status = #{status} WHERE id = #{id} AND user_id = #{userId}")
    @Nullable
    Task changeOneStatusById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final Status status
    );

    @Update("UPDATE TABLE 'task' SET status = #{status} WHERE name = #{name} AND user_id = #{userId}")
    @Nullable
    Task changeOneStatusByName(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final Status status
    );

    @Delete("DELETE FROM 'task' WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    void removeById(@NotNull String userId,@NotNull String id);

    @Delete("DELETE FROM 'task' WHERE name = #{name} AND user_id = #{userId} LIMIT 1")
    void removeOneByName(@NotNull String userId,@NotNull String name);

    @Update("UPDATE TABLE 'task' SET name = #{name}, description = #{description}"
            + " WHERE id = #{id} AND user_id = #{userId}")
    @Nullable
    Task updateOneById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    );

}
