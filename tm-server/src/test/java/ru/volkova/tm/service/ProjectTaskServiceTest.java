package ru.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.IPropertyService;
import ru.volkova.tm.api.repository.IProjectRepository;
import ru.volkova.tm.api.repository.ITaskRepository;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.IProjectTaskService;
import ru.volkova.tm.api.service.ITaskService;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.marker.UnitCategory;

import java.util.List;

public class ProjectTaskServiceTest {

    private final IPropertyService propertyService = new PropertyService();

    private final IConnectionService connectionService = new ConnectionService(propertyService);

    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @NotNull
    private final User user = new User();

    @Test
    @Category(UnitCategory.class)
    public void bindTaskByProjectIdTest() {
        final Project project = new Project();
        final Task task = new Task();
        task.setUserId(user.getId());
        taskService.insert(task);
        projectTaskService.bindTaskByProjectId(task.getUserId(), project.getId(), task.getId());
        Assert.assertNotNull(task.getProjectId());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllByProjectIdTest() {
        final Project project = new Project();
        project.setUserId(user.getId());
        final Task task1 = new Task();
        final Task task2 = new Task();
        task1.setUserId(user.getId());
        task2.setUserId(user.getId());
        taskService.insert(task1);
        taskService.insert(task2);
        projectTaskService.bindTaskByProjectId(user.getId(), project.getId(), task1.getId());
        projectTaskService.bindTaskByProjectId(user.getId(), project.getId(), task2.getId());
        List<Task> taskList = projectTaskService.findAllTasksByProjectId(project.getUserId(), project.getId());
        Assert.assertNotNull(taskList.contains(task1));
        Assert.assertTrue(taskList.contains(task2));
    }

    @Test
    @Category(UnitCategory.class)
    public void unbindTaskByProjectIdTest() {
        final Project project = new Project();
        final Task task = new Task();
        task.setUserId(user.getId());
        taskService.insert(task);
        projectTaskService.bindTaskByProjectId(task.getUserId(), project.getId(), task.getId());
        projectTaskService.unbindTaskByProjectId(task.getUserId(), project.getId(), task.getId());
        Assert.assertNull(task.getProjectId());
    }

}
